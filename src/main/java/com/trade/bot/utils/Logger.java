package com.trade.bot.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class Logger {
    private static String logFilePath = ".\\log.txt";
    private static BufferedWriter bufferedReader = null;

    public static void log(Object o){
        String logMessagePrefix = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(System.currentTimeMillis()) +" MESSAGE: ";

        System.out.println(o);

        try {
            bufferedReader = new BufferedWriter(new FileWriter(logFilePath, true));

            if(o.toString().contains("\n")) bufferedReader.newLine();
            bufferedReader.write(logMessagePrefix + o.toString());
            bufferedReader.newLine();
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
