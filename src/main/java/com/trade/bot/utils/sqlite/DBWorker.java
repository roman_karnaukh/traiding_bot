package com.trade.bot.utils.sqlite;

import com.trade.bot.brokers.exmo.models.Ticker;
import com.trade.bot.utils.Logger;

import java.sql.*;
import java.text.SimpleDateFormat;

public class DBWorker {
    private static Connection conn;
    private static Statement statmt;
    private static ResultSet resSet;

    public static void connect() throws ClassNotFoundException, SQLException {
        conn = null;
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:TEST1.s3db");

        Logger.log("DB is connected!");
    }

    public static void createDB() throws SQLException {
        statmt = conn.createStatement();

        statmt.execute("CREATE TABLE if not exists 'tickets' " +
                "('id' INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "'pair' TEXT, " +
                "'buy_price' REAL, " +
                "'sell_price' REAL, " +
                "'last_trade' REAL, " +
                "'high' REAL, " +
                "'low' REAL, " +
                "'avg' REAL, " +
                "'vol' REAL, " +
                "'vol_curr' REAL, " +
                "'updated' INTEGER" +
                ");");
//"BTC_USD": {
//        "buy_price": "589.06",
//        "sell_price": "592",
//        "last_trade": "591.221",
//        "high": "602.082",
//        "low": "584.51011695",
//        "avg": "591.14698808",
//        "vol": "167.59763535",
//        "vol_curr": "99095.17162071",
//        "updated": 1470250973
//        }
        Logger.log("Table is created or already exists");
    }

    public static void addTicker(String pair, Ticker ticker) {
        try {
            statmt.execute("INSERT INTO 'tickets' " +
                    "('pair', 'buy_price', 'sell_price', 'last_trade', 'high', 'low', 'avg', 'vol', 'vol_curr', 'updated') " +
                    "VALUES " +
                    "('" + pair +"', " +
                    ticker.getBuy_price() + "," +
                    ticker.getSell_price() + "," +
                    ticker.getLast_trade() + "," +
                    ticker.getHigh() + "," +
                    ticker.getLow() + "," +
                    ticker.getAvg() + "," +
                    ticker.getVol() + "," +
                    ticker.getVol_curr() + "," +
                    ticker.getUpdated() + "); ");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void readDB() throws SQLException {

        resSet = statmt.executeQuery("SELECT * FROM tickets");
        while(resSet.next()){
            int id = resSet.getInt("id");
            String pair = resSet.getString("pair");
            double buy_price = resSet.getDouble("buy_price");
            double sell_price = resSet.getDouble("sell_price");
            double last_trade = resSet.getDouble("last_trade");
            double high = resSet.getDouble("high");
            double low = resSet.getDouble("low");
            double avg = resSet.getDouble("avg");
            double vol = resSet.getDouble("vol");
            double vol_curr = resSet.getDouble("vol_curr");
            int updated = resSet.getInt("sell_price");

            Logger.log("ID = " + id);
            Logger.log("pair = " + pair);
            Logger.log("buy_price = " + buy_price);
            Logger.log("sell_price = " + sell_price);
            Logger.log("last_trade = " + last_trade);
            Logger.log("high = " + high);
            Logger.log("low = " + low);
            Logger.log("avg = " + avg);
            Logger.log("vol = " + vol);
            Logger.log("vol_curr = " + vol_curr);
            Logger.log("updated = " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(updated));
            Logger.log("");
        }

        Logger.log("Table is viewed");
    }

    public static void closeDB() throws SQLException {
        conn.close();
        resSet.close();

        Logger.log("Connections are closed");
    }
}
