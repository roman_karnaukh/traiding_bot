package com.trade.bot.models;

public class CurrencyPair {
    private String currencyPair;
    public CurrencyPair(String currencyPair){
        this.currencyPair = currencyPair;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    @Override
    public String toString() {
        return currencyPair;
    }
}
