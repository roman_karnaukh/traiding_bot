package com.trade.bot.models;

import java.text.SimpleDateFormat;

public class Order {
    private long oderDate;
    private CurrencyPair pair;
    private double orderRate;
    private double upperStopRate;
    private double lowerStopRate;
    private double marketRate;
    private double count;
    private double result;
    private boolean isProfit;

    public double getOrderRate() {
        return orderRate;
    }

    public long getOderDate() {
        return oderDate;
    }

    public double getLowerStopRate() {
        return lowerStopRate;
    }

    public double getUpperStopRate() {
        return upperStopRate;
    }

    public void setOderDate(long oderDate) {
        this.oderDate = oderDate;
    }

    public void setOrderRate(double orderRate) {
        this.orderRate = orderRate;
    }

    public void setLowerStopRate(double lowerStopRate) {
        this.lowerStopRate = lowerStopRate;
    }

    public void setUpperStopRate(double upperStopRate) {
        this.upperStopRate = upperStopRate;
    }

    public CurrencyPair getPair() {
        return pair;
    }

    public void setPair(CurrencyPair pair) {
        this.pair = pair;
    }

    public void setPair(String pair) {
        this.pair = new CurrencyPair(pair);
    }

    public double getMarketRate() {
        return marketRate;
    }

    public void setMarketRate(double marketRate) {
        this.marketRate = marketRate;
    }


    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return getPair() +":" + getOrderRate() + "*" + getCount() +
                " TIME:" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(getOderDate()) +
                " L_STOP:" + getLowerStopRate() +
                " U_STOP:" + getUpperStopRate() +
                " LAST_RATE:" + getMarketRate();
    }

    public double calculateResult(){
        setResult(((getMarketRate() - getOrderRate()) * getCount()));
        setIsProfit(getResult() >= 0);

        return this.getResult();
    }

    public boolean getIsProfit() {
        return isProfit;
    }

    public void setIsProfit(boolean isProfit) {
        this.isProfit = isProfit;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }
}
