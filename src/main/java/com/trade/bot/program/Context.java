package com.trade.bot.program;

import com.trade.bot.interfaces.IBroker;
import com.trade.bot.models.Order;

public class Context {
    private static Order Order;
    private static IBroker Broker;

    public static Order getOrder(){
        if (Context.Order==null) {
            Context.Order = new Order();
            Context.Order.setPair(Broker.getProperties().getCurrency());
        }

        return Context.Order;
    }

    public static void clearOrder(){
        Context.Order = null;
    }

    public static void setBroker(IBroker broker){
        Context.Broker = broker;
    }

    public static IBroker getBroker(){
        return Context.Broker;
    }
}
