package com.trade.bot.program;

import com.trade.bot.brokers.exmo.EXMOBroker;
import com.trade.bot.brokers.exmo.EXMOBrokerService;
import com.trade.bot.interfaces.IStrategy;
import com.trade.bot.strategies.SimpleStrategy;
import com.trade.bot.utils.Logger;
import com.trade.bot.utils.sqlite.DBWorker;

import java.sql.SQLException;
import java.util.HashMap;


public class Program {
    private static IStrategy str = new SimpleStrategy(new EXMOBroker());

    public static void main(String[] args) throws InterruptedException, SQLException, ClassNotFoundException {
        String times = args.length==0 ? "20" : args[0];
        String sleep = args.length==0 ? "3000" : args[1];

        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            str.urgentSell();
            try {
                DBWorker.closeDB();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }));

        DBWorker.connect();
        DBWorker.createDB();
        DBWorker.readDB();

        for (int i = 0; i < Integer.valueOf(times); i++) {
            str.check();

            Thread.sleep(Long.valueOf(sleep));
        }


        EXMOBrokerService.AuthenticatedAPI a = new EXMOBrokerService.AuthenticatedAPI(Context.getBroker().getProperties().getApiKey(),
                Context.getBroker().getProperties().getSecretKey());

        String result = a.Request("user_info", null);
        Logger.log("\nuser_info: " + result);

        String result2 = a.Request("user_cancelled_orders", new HashMap<String, String>() {{
            put("limit", "2");
            put("offset", "0");
        }});
        Logger.log("\nuser_cancelled_orders: " + result2);


        String result3 = a.Request("order_create", new HashMap<String, String>(){{
            put("pair", "XRP_USD");
            put("quantity", "15");
            put("price", "2.85");
            put("type", "buy");

        }});

        Logger.log("\norder_create: " + result3);

        String result4 = a.Request("order_cancel", new HashMap<String, String>() {{
            put("order_id", "155565");
        }});
        Logger.log("\norder_cancel: " + result4);

        String result5 = a.Request("user_open_orders", null);
        Logger.log("\nuser_open_orders: " + result5);

        String result6 = a.Request("user_trades", new HashMap<String, String>() {{
            put("pair", "XRP_USD");
            put("offset", "2");
            put("limit", "1000");
        }});
        Logger.log("\nuser_trades: " + result6);

        String result7 = a.Request("user_cancelled_orders", new HashMap<String, String>() {{
            put("offset", "2");
            put("limit", "1000");
        }});
        Logger.log("\nuser_cancelled_orders: " + result7);

        String result8 = a.Request("order_trades", new HashMap<String, String>() {{
            put("order_id", "156865");
        }});
        Logger.log("\norder_trades: " + result8);

        String result9 = a.Request("required_amount", new HashMap<String, String>() {{
            put("pair", "XRP_USD");
            put("quantity", "15");
        }});
        Logger.log("\nrequired_amount: " + result9);

        String result10 = a.Request("deposit_address", null);
        Logger.log("\ndeposit_address: " + result10);


    }
}
