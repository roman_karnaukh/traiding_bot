package com.trade.bot.strategies;

import com.trade.bot.program.Context;
import com.trade.bot.constants.TradeCommand;
import com.trade.bot.interfaces.IBroker;
import com.trade.bot.interfaces.IStrategy;
import com.trade.bot.utils.Logger;

import java.util.concurrent.TimeUnit;

public class SimpleStrategy  implements IStrategy {
    private IBroker broker;
    private double count = 15;

    public SimpleStrategy(IBroker broker) {
        this.broker = broker;

        Context.setBroker(this.broker);
    }

    public TradeCommand calculate() {
        double current = broker.getCurrentSellValue();
        Context.getOrder().setMarketRate(current);

        if(Context.getOrder().getOrderRate() == 0) {
            System.out.println("OrderRate == 0");
            return TradeCommand.BYE;
        } else if(isTimeOutAchieved()){
            System.out.println("TimeOut Achieved");
            return TradeCommand.SELL;
        } else if(isLowerStopAchieved()){
            System.out.println("LowerStop Achieved");
            return TradeCommand.SELL;
        } else if (isUpperStopAchieved()){
            System.out.println("UpperStop Achieved");
            Context.getOrder().setLowerStopRate(Context.getOrder().getLowerStopRate() + broker.getProperties().getByeStopIteration());
            Context.getOrder().setUpperStopRate(Context.getOrder().getUpperStopRate() + broker.getProperties().getByeStopIteration());
            return TradeCommand.KEEP;
        } else {
            return TradeCommand.KEEP;
        }
    }

    public IBroker getBroker() {
        return this.broker;
    }

    @Override
    public void check() {
        switch (this.calculate()){
            case BYE:
                Logger.log("trying to bye " + Context.getOrder());

                broker.bye(count);
                break;
            case KEEP:

                Logger.log("keeping " + Context.getOrder());
                break;
            case SELL:
                Logger.log("selling " + Context.getOrder());

                count = broker.sellOrder() ? 15 : count * 2;
                break;
            default:
                break;
        }
    }

    @Override
    public void urgentSell() {
        broker.sellOrder();
        broker.getProperties().writeProperties();
    }

    private boolean isTimeOutAchieved(){
        return (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - TimeUnit.MILLISECONDS.toSeconds(Context.getOrder().getOderDate()))
                > broker.getProperties().getOrderTimeout();
    }

    private boolean isLowerStopAchieved(){
        return Context.getOrder().getMarketRate() <= Context.getOrder().getLowerStopRate();
    }

    private boolean isUpperStopAchieved(){
        return Context.getOrder().getMarketRate() >= Context.getOrder().getUpperStopRate();
    }
}
