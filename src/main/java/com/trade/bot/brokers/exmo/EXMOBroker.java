package com.trade.bot.brokers.exmo;

import com.trade.bot.program.Context;
import com.trade.bot.brokers.exmo.models.Ticker;
import com.trade.bot.interfaces.IBroker;
import com.trade.bot.interfaces.IBrokerProp;
import com.trade.bot.interfaces.IWallet;
import com.trade.bot.models.CurrencyPair;

public class EXMOBroker implements IBroker {
    private EXMOBrokerService service = new EXMOBrokerService();
    private IBrokerProp properties = new EXMOProperties();
    private IWallet wallet = new EXMOWallet();

    public double getCurrentSellValue() {
        return service.getStatistics(Context.getOrder().getPair().getCurrencyPair()).getBuy_price();
    }

    @Override
    public double getCurrentByeValue() {
        return service
                .getStatistics(Context.getOrder().getPair().getCurrencyPair())
                .getBuy_price();
    }

    public double[] getPairHistoryByPeriod(CurrencyPair currencyPair, String startDay, String endDay) {
        return new double[0];
    }

    public IWallet getWallet() {
        return wallet;
    }

    public void bye(double count) {
        Ticker orderData = service.makeOrder();

        Context.getOrder().setCount(count);
        Context.getOrder().setOderDate(System.currentTimeMillis());
        Context.getOrder().setOrderRate(orderData.getSell_price());
        Context.getOrder().setUpperStopRate(orderData.getBuy_price() + properties.getByeStopIteration());
        Context.getOrder().setLowerStopRate(orderData.getBuy_price() - properties.getSellStopIteration());
    }

    public boolean sellOrder() {
        boolean isProfit;

        service.sellOrder(Context.getOrder());
        this.wallet.implementChange(Context.getOrder().calculateResult());
        isProfit = Context.getOrder().getIsProfit();

        Context.clearOrder();
        return isProfit;
    }

    public boolean authorize() {
        return true;
    }

    public IBrokerProp getProperties() {
        return properties;
    }
}
