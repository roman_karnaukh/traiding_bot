package com.trade.bot.brokers.exmo.models;

public class Ticker {
    private double buy_price;
    private double sell_price;
    private double last_trade;
    private double high;
    private double low;
    private double avg;
    private double vol;
    private double vol_curr;
    private int updated;

    public double getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(double buy_price) {
        this.buy_price = buy_price;
    }

    public double getSell_price() {
        return sell_price;
    }

    public void setSell_price(double sell_price) {
        this.sell_price = sell_price;
    }

    public double getLast_trade() {
        return last_trade;
    }

    public void setLast_trade(double last_trade) {
        this.last_trade = last_trade;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public double getVol() {
        return vol;
    }

    public void setVol(double vol) {
        this.vol = vol;
    }

    public double getVol_curr() {
        return vol_curr;
    }

    public void setVol_curr(double vol_curr) {
        this.vol_curr = vol_curr;
    }

    public int getUpdated() {
        return updated;
    }

    public void setUpdated(int updated) {
        this.updated = updated;
    }

}

//"BTC_USD": {
//        "buy_price": "589.06",
//        "sell_price": "592",
//        "last_trade": "591.221",
//        "high": "602.082",
//        "low": "584.51011695",
//        "avg": "591.14698808",
//        "vol": "167.59763535",
//        "vol_curr": "99095.17162071",
//        "updated": 1470250973
//        }