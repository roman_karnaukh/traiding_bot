package com.trade.bot.brokers.exmo;

import com.trade.bot.brokers.exmo.models.Ticker;
import com.trade.bot.models.Order;
import com.trade.bot.utils.Logger;
import com.trade.bot.utils.sqlite.DBWorker;
import okhttp3.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class EXMOBrokerService {
    private String host = "https://api.exmo.com/";
    private Ticker lastTicker;

    public Ticker getStatistics(String pair){
        lastTicker = getStatistics().get(pair);

        DBWorker.addTicker(pair, lastTicker);

        return lastTicker;
    }

    public HashMap<String, Ticker> getStatistics(){
        HashMap<String, Ticker> map = null;
        String url = host + "v1/ticker/";

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        HttpResponse response = null;

        try {
            response = client.execute(request);

            Logger.log("\nSending 'GET' request to URL : " + url);
            Logger.log("Response Code : " +
                    response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            map = new ObjectMapper().readValue(result.toString(), new TypeReference<HashMap<String, Ticker>>(){});

        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

    public Ticker makeOrder() {
        return lastTicker;
    }

    public boolean sellOrder(Order order) {

        return true;
    }

    public static class AuthenticatedAPI {
        private static long _nonce;
        private String _key;
        private String _secret;

        public AuthenticatedAPI(String key, String secret) {
            _nonce = System.nanoTime();
            _key = key;
            _secret = secret;
        }

        public final String Request(String method, Map<String, String> arguments) {
            Mac mac;
            SecretKeySpec key;
            String sign;

            if (arguments == null) {  // If the user provided no arguments, just create an empty argument array.
                arguments = new HashMap<>();
            }

            arguments.put("nonce", "" + ++_nonce);  // Add the dummy nonce.

            String postData = "";

            for (Map.Entry<String, String> stringStringEntry : arguments.entrySet()) {
                Map.Entry argument = (Map.Entry) stringStringEntry;

                if (postData.length() > 0) {
                    postData += "&";
                }
                postData += argument.getKey() + "=" + argument.getValue();
            }

            // Create a new secret key
            try {
                key = new SecretKeySpec(_secret.getBytes("UTF-8"), "HmacSHA512");
            } catch (UnsupportedEncodingException uee) {
                System.err.println("Unsupported encoding exception: " + uee.toString());
                return null;
            }

            // Create a new mac
            try {
                mac = Mac.getInstance("HmacSHA512");
            } catch (NoSuchAlgorithmException nsae) {
                System.err.println("No such algorithm exception: " + nsae.toString());
                return null;
            }

            // Init mac with key.
            try {
                mac.init(key);
            } catch (InvalidKeyException ike) {
                System.err.println("Invalid key exception: " + ike.toString());
                return null;
            }


            // Encode the post data by the secret and encode the result as base64.
            try {
                sign = Hex.encodeHexString(mac.doFinal(postData.getBytes("UTF-8")));
            } catch (UnsupportedEncodingException uee) {
                System.err.println("Unsupported encoding exception: " + uee.toString());
                return null;
            }

            // Now do the actual request
            MediaType form = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

            OkHttpClient client = new OkHttpClient();
            try {

                RequestBody body = RequestBody.create(form, postData);
                Request request = new Request.Builder()
                        .url("https://api.exmo.com/v1/" + method)
                        .addHeader("Key", _key)
                        .addHeader("Sign", sign)
                        .post(body)
                        .build();

                Response response = client.newCall(request).execute();

                Logger.log("\nSending 'POST' request to URL : " + request.url());
                Logger.log("Response Code : " + response.code());
                return response.body().string();
            } catch (IOException e) {
                System.err.println("Request fail: " + e.toString());
                return null;  // An error occured...
            }
        }
    }
}
