package com.trade.bot.brokers.exmo;

import com.trade.bot.interfaces.IBrokerProp;

import java.io.*;
import java.util.Properties;

public class EXMOProperties implements IBrokerProp {
    private Properties properties;
    private String externalPath = ".\\exmoBroker.properties";

    EXMOProperties() {
        try {
            InputStream inputStream;
            properties = new Properties();

            inputStream = (new File(externalPath).exists()) ?
                    new FileInputStream(externalPath) :
                    getClass().getResourceAsStream("/exmoBroker.properties");

            properties.load(inputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCurrency(){
      return properties.getProperty("currency");
    }

    public String getEmail(){
        return properties.getProperty("email");
    }

    public String getPassword(){
        return properties.getProperty("password");
    }

    public double getByeStopIteration() {
        return Double.valueOf(properties.getProperty("low_limit"));
    }

    public double getSellStopIteration() {
        return Double.valueOf(properties.getProperty("high_limit"));
    }

    public long getOrderTimeout() {
        return Long.valueOf(properties.getProperty("order_timeout"));
    }

    public String getApiKey(){
        return properties.getProperty("api_key");
    }

    public String getSecretKey(){
        return properties.getProperty("secret_key");
    }

    @Override
    public int getMinLimit() {
        return Integer.parseInt(properties.getProperty("min_limit"));
    }

    @Override
    public int getMaxLimit() {
        return Integer.parseInt(properties.getProperty("max_limit"));
    }

    @Override
    public double getUsdValue() {
        return Double.valueOf(properties.getProperty("usd"));
    }

    @Override
    public void setUsdValue(double v) {
        properties.setProperty("usd", String.valueOf(v));
        properties.put("usd_sell", String.valueOf(v));
    }

    public void writeProperties(){
        FileWriter fileWriter;
        try {
            fileWriter  =  new FileWriter(externalPath);

            properties.store(fileWriter, "Comment");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
