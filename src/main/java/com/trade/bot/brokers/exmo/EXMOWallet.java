package com.trade.bot.brokers.exmo;

import com.trade.bot.program.Context;
import com.trade.bot.interfaces.IWallet;
import com.trade.bot.models.WalletCurrency;

public class EXMOWallet implements IWallet {

    @Override
    public WalletCurrency[] getWalletCurrencies() {
        return new WalletCurrency[0];
    }

    @Override
    public WalletCurrency getWalletCurrency(WalletCurrency currency) {
        return null;
    }

    public void implementChange(double v){
        double current = Context.getBroker().getProperties().getUsdValue();
        Context.getBroker().getProperties().setUsdValue(current + v);
    }
}
