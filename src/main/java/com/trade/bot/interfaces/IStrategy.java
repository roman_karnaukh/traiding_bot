package com.trade.bot.interfaces;

import com.trade.bot.constants.TradeCommand;
import com.trade.bot.models.Order;

public interface IStrategy {
    TradeCommand calculate();
    IBroker getBroker();

    void check();
    void urgentSell();
}
