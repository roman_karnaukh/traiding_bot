package com.trade.bot.interfaces;

import com.trade.bot.models.WalletCurrency;

public interface IWallet {
    WalletCurrency[] getWalletCurrencies();
    WalletCurrency getWalletCurrency(WalletCurrency currency);
    void implementChange(double v);
}
