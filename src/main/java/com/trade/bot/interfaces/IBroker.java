package com.trade.bot.interfaces;

import com.trade.bot.models.CurrencyPair;

public interface IBroker {
    double getCurrentSellValue();
    double getCurrentByeValue();
    double[] getPairHistoryByPeriod(CurrencyPair currencyPair, String startDay, String endDay);
    IWallet getWallet();
    boolean authorize();
    IBrokerProp getProperties();
    void bye(double count);
    boolean sellOrder();
}
