package com.trade.bot.interfaces;

public interface IBrokerProp {
    String getCurrency();
    String getEmail();
    String getPassword();
    double getByeStopIteration();
    double getSellStopIteration();
    long getOrderTimeout();
    String getApiKey();
    String getSecretKey();
    int getMinLimit();
    int getMaxLimit();
    double getUsdValue();
    void setUsdValue(double v);
    void writeProperties();
}
